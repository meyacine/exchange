<?php
const LOG_FOLDER = '../../log/';
// check variables
if (isset($_GET['money']) && isset($_GET['price'])) {
    extract($_GET);
    $moneyFolder = LOG_FOLDER . $money . '/';
    if (!file_exists($moneyFolder)) {
        mkdir($moneyFolder, 0777, true);
    }
    // format fileName
    $fileName = $moneyFolder . date('Y_m_d') . '.csv';
    // format data
    $data = $money . ',' . date('Y/m/d H:i:s') . ',' . $price . PHP_EOL;
    file_put_contents($fileName, $data, FILE_APPEND | LOCK_EX);
} else {
    // 400 for bad request
    http_response_code(400);
}


