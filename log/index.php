<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../libs/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../app/style.css">
    <title>Exchange log</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Exchange</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="../">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="./log">Log <span class="sr-only">(current)</span></a>
            </li>
        </ul>
    </div>
</nav>
<?php
function getDirContents($dir, &$results = array()){
    $files = scandir($dir);

    foreach($files as $key => $value){
        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
        if (strpos($path,'index.php')) {
            continue;
        }
        if(!is_dir($path)) {
            $results[] = $path;
            echo "<li><a href='$path'>$path</a></li>";
        } else if($value != "." && $value != "..") {
            echo "<li>$path<ul>";
            getDirContents($path, $results);
            $results[] = $path;
            echo "</ul></li>";
        }
    }
    return $results;
}
echo "<ul>";
getDirContents('./');
echo "</ul>";
?>
<script src="../libs/jquery.min.js"></script>
<script src="../libs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
